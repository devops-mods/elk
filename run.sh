#!/bin/bash

FILE=./config/env
ES_ENV=./config/elasticsearch/env
LOGSTASH_ENV=./config/logstash/env
KIBANA_ENV=./config/kibana/env
FILEBEAT_ENV=./config/filebeat/env

function ask_jvm_ram() {
  local name=$1
  local JVM_RAM_MIN
  local JVM_RAM_MAX

  while true
    do
      read -p "Set $name JVM min ram (min 256 mb): " JVM_RAM_MIN
      if [[ ${JVM_RAM_MIN} -ge 256 ]]
      then
        while true
        do
          read -p "Set $name JVM max ram (greater or equal than $JVM_RAM_MIN mb): " JVM_RAM_MAX
          if [[ ${JVM_RAM_MAX} -ge ${JVM_RAM_MIN} ]]
          then
            JVM_RAM="-Xmx${JVM_RAM_MAX}m -Xms${JVM_RAM_MIN}m"
            break
          fi
        done
        break
      fi
    done
}
function init() {
  if [ ! -f "$FILE" ]; then
  while true
  do
      read -p "Set your domain name: " domain
      echo
      if [[ "$domain" =~ ^[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$ ]]
      then
          echo "domain=$domain" > $FILE
          break
      else
          echo "Domain address $domain is invalid."
      fi
  done
  fi
  if [ ! -f "$ES_ENV" ]; then
  while true
  do
      while true
      do
        read -p "Set password for elastic user: " es_password
        echo
        if [[ ${#es_password} -ge 8 ]]
        then
          echo "ELASTIC_PASSWORD=$es_password" > $ES_ENV
          echo "ELASTIC_PASSWORD=$es_password" > $LOGSTASH_ENV
          echo "ELASTIC_PASSWORD=$es_password" > $KIBANA_ENV
          echo "ELASTIC_PASSWORD=$es_password" > $FILEBEAT_ENV
          break
        else
            echo "Password must contain at least 8 characters"
        fi
      done
      ask_jvm_ram "elasticsearch"
      echo "ES_JAVA_OPTS=$JVM_RAM"  >> $ES_ENV
      ask_jvm_ram "logstash"
      echo "LS_JAVA_OPTS=$JVM_RAM"  >> $LOGSTASH_ENV
      break
  done
  fi
}

function start() {
    export $(grep -v '^#' $FILE | xargs)
    docker-compose up -d
    if [ $? -eq 0 ]
    then
      echo -e "\e[92m Stack successfully start. \n
      Url: https://kibana.$domain \e[39m"
    fi
}

function reset() {
  rm ./config/env
  rm ./config/*/env
}

function clean() {
    reset
    docker-compose down -v
}

function main() {
  init
  start
}
HELP="
\e[92m Use ./run.sh to start stack \n
\e[31m -c or --clean to clean config files and docker volumes and networks \n
\e[34m -r or --reset to purge config files \n
\e[39m -h or --help to display help"

if [[ "$#" -eq 0 ]]
then
  main
fi

while [[ "$#" -gt 0 ]]; do
  case $1 in
      -c|--clean) clean; shift ;;
      -r|--reset) reset; shift;;
      -h|--help) echo -e $HELP; exit;;
      *) echo "Unknown parameter passed: $1"; exit 1 ;;
  esac
  shift
done

