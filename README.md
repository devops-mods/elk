# Elk

## How to install
Set the permission to execute run.sh
```console
chmod u+x run.sh
```
Execute run.sh
```console
./run.sh
```

## What do run.sh

1. Configure the domain name for kibana
2. Configure elasticsearch password
3. Set memories for elasticsearch and logstash JVM
3. Start docker-compose.yml stack

### How to use run.sh

```console
 Use ./run.sh to start stack 
  -c or --clean to clean config files and docker volumes and networks 
  -r or --reset to purge config files 
  -h or --help to display help
```
